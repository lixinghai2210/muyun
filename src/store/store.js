import Vue from 'vue'
import Vuex from 'vuex'

//挂载Vuex
Vue.use(Vuex)

//创建VueX对象
const store = new Vuex.Store({
    state:{
        //存放的键值对就是所要管理的状态
        name:'helloVueX',
        proCode:'-1',
        // 状态
        Status:'-1',
        // 级别
        Level:'-1',
        // 发布人
        Publisher:'-1',
        // 负责人
        Responsible:'-1',
        // 协同人
        Coordinator:'-1',
        //
        Search:'',
        setDate1:'',
        setDate2:'',
        //页码
        pagenum:1,
        // 页条数
        pagesize:10,
        // 未读消息条数
        readNum:'',
    },
    mutations:{
      getReadNum(state,num){
        if (num == 0) {
          num = "";
        }
        state.readNum = num
      },
      // 项目code
      projectCode(state,proCode){
          state.proCode = proCode
      },
      // 状态
      Status(state,Status){
        state.Status = Status
      },
      Level(state,Level){
        state.Level=Level
      },
      // 发布人
      Publisher(state,Publisher){
        state.Publisher= Publisher
      },
      // 负责人
      Responsible(state,Responsible){
        state.Responsible= Responsible
      },
      // 协同人
      Coordinator(state,Coordinator){
        state.Coordinator= Coordinator
      },
      Search(state,Search){
        state.Search = Search
      },
      setDate1(state,setDate1){
        state.setDate1 =setDate1
      },
      setDate2(state,setDate2){
        state.setDate2 =setDate2
      },
      pagenum(state,pagenum){
        state.pagenum =pagenum
      },
      pagesize(state,pagesize){
        state.pagesize =pagesize
      }

  }
})

export default store