import Vue from 'vue'
import Router from 'vue-router'
import Login from '../views/login.vue'
import Home from '../views/home.vue'
import Index from '../views/index.vue'
import NotFound from '../views/404.vue';
import welcome from '@/views/welcome'


import devicelist from '@/views/Device.vue/devicelist'
import adddevice from '@/views/Device.vue/adddevice'
import recordslist from '@/views/TestRecords/recordslist'
import addrecord from '@/views/TestRecords/addrecord'
import chart from '@/views/TestRecords/chart'




import editPwd from '@/views/Setup/editPwd'
import peopleInfo from '@/views/Setup/peopleInfo'
import productList from '@/views/Warehouse/Product/productList'
import warehouseList from '@/views/Warehouse/Warehouse/warehouseList'
import addWarehouse from '@/views/Warehouse/Warehouse/addWarehouse'
import editWarehouse from '@/views/Warehouse/Warehouse/editWarehouse'
import detailWarehouse from '@/views/Warehouse/Warehouse/detailWarehouse'
import addShelf from '@/views/Warehouse/Warehouse/Shelf/addShelf'
import detailShelf from '@/views/Warehouse/Warehouse/Shelf/detailShelf'
import editShelf from '@/views/Warehouse/Warehouse/Shelf/editShelf'
import addTag from '@/views/Warehouse/Warehouse/Shelf/Tag/addTag'
import editTag from '@/views/Warehouse/Warehouse/Shelf/Tag/editTag'
import logList from '@/views/Warehouse/InOutStock/logList'
import detailProductLog from '@/views/Warehouse/Product/detailProductLog'
import inStock from '@/views/Warehouse/Product/inStock'
import outStock from '@/views/Warehouse/Product/outStock'
import addStockProduct from '@/views/Warehouse/Product/addStockProduct'

import systemLog from '@/views/Setup/systemLog'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/login', component: Login
    },
    { path: '/', redirect: '/welcome' },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      redirect: '/projectlist',
      children: [
        // {
        //   path: '/index',
        //   component: Index,
        // },

        //设备列表
        { 
          path: '/devicelist', component: devicelist, 
        },
        { 
          path: '/adddevice', component: adddevice, 
        },
        //检测记录列表
        { 
          path: '/recordslist', component: recordslist, 
        },
        { 
          path: '/addrecord', component: addrecord, 
        },
        { 
          path: '/chart', component: chart, 
        },




        
        //仓库管理
        { 
          path: '/productList', component: productList, 
        },
        { 
          path: '/warehouseList', component: warehouseList, 
        },
        { 
          path: '/addWarehouse', component: addWarehouse, 
        },
        { 
          path: '/editWarehouse', component: editWarehouse, 
        },
        { 
          path: '/detailWarehouse', component: detailWarehouse, 
        },
        //货架
        { 
          path: '/addShelf', component: addShelf, 
        },
        { 
          path: '/detailShelf', component: detailShelf, 
        },
        { 
          path: '/editShelf', component: editShelf, 
        },
        //位置标签
        { 
          path: '/addTag', component: addTag, 
        },
        { 
          path: '/editTag', component: editTag, 
        },
        //出入库
        { 
          path: '/logList', component: logList, 
        },
        { 
          path: '/detailProductLog', component: detailProductLog, 
        },
        { 
          path: '/inStock', component: inStock, 
        },
        { 
          path: '/outStock', component: outStock, 
        },
        {
          path: '/addStockProduct',component : addStockProduct,
        },

      
        //设置
        {
          path: '/editPwd', component: editPwd, 
        },
        {
          path: '/peopleInfo', component: peopleInfo, 
        },
    
        {
          path: '/welcome', component:  welcome, 
        },
        {
          path: '/systemLog' ,component: systemLog,
        },
        //最后一个404跳转
        {
          path: "/404",
          component: NotFound,
        }
      ]
    },
  ]
})

//挂在路由导航守卫
// router.beforeEach((to, from, next) => {
//   if (to.path === '/login') return next()
//   const tokenStr = window.localStorage.getItem("uKey")
//   if (!tokenStr) return next('/login')
//   next();
//   //错误的url 跳404
//   if (to.matched.length !== 0) {
//     next()
//   } else {
//     next({
//       path: '/welcome'
//     })
//   }
// })

export default router
