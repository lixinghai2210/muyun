// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import echarts from 'echarts'
import './assets/icon/iconfont.css'
import '../src/assets/css/glogal.css'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import axios from 'axios'
import VueAxios from 'vue-axios'
import 'lib-flexible'
import Sort from './tool/sort'
import TimeFormat from './tool/myDate'
import NumRules from './tool/numRules'
import file from './tool/file.js'
import store from './store/store'
import filter from '@/tool/filter'
import VueContextMenu from 'vue-contextmenu'

//路由跳转router 3.0错误解决↓↓↓↓↓
import Router from 'vue-router'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}
///↑↑↑↑↑↑

// axios.defaults.baseURL = '/hxapi/' //axios默认请求头
Vue.prototype.$echarts = echarts
Vue.config.productionTip = false
Vue.use(VueAxios,axios);
Vue.use(ElementUI);
Vue.prototype.fileFormat=file;
Vue.prototype.Sort=Sort;
Vue.prototype.TimeFormat=TimeFormat;
Vue.prototype.NumRules  = NumRules;
Vue.use(VueContextMenu)

/* eslint-disable no-new */
// 这种方式可以将filters.js中的所有过滤器都注册
for (let key in filter) {
  Vue.filter(key, filter[key])
}


new Vue({
  el: '#app',
  router,
  store,  //store:store 和router一样，将我们创建的Vuex实例挂载到这个vue实例中
  components: { App },
  template: '<App/>'
})
