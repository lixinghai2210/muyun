export default
    {
        // 正整数
        number(value) {
            value = value.replace(/[^\.\d]/g, "");//除点和数字外置空
            value = value.replace(".", "");//点置空
            value = value.replace(/^0/g, "");//第一位为0置空
            return value;
        },
        // 金额数字格式
        numMoney(value) {
            value = value.match(
                /\d+(\.\d{0,2})?/
            )
                ? value.match(/\d+(\.\d{0,2})?/)[0]
                : "";
            return value;
        },
    }