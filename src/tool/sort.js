
export default
    {
        // 按照一个字段数值排序
        compare(property) {
            return function (a, b) {
                var value1 = a[property];
                var value2 = b[property];
                return value1 - value2;
            }
        },
        // data为数组，p为属性名
        // 中文正向排序
        ForwardRanking(data, p) {
            for (var i = 0; i < data.length - 1; i++) {
                for (var j = 0; j < data.length - 1 - i; j++) {
                    var dd = (data[j][p].localeCompare(data[j + 1][p], "zh"));//1---前者往后移，-1===位置不变
                    if (dd > 0) {
                        var temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
            return data;
        },
        // 中文反向排序
        ReverseRanking(data, p) {
            for (var i = 0; i < data.length - 1; i++) {
                for (var j = 0; j < data.length - 1 - i; j++) {
                    var dd = (data[j][p].localeCompare(data[j + 1][p], "zh"));//1---前者往后移，-1===位置不变
                    if (dd < 0) {
                        var temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
            return data;
        },
        // 日期正序
        SortRankingDate(data, p) {
            for (var i = 0; i < data.length - 1; i++) {
                for (var j = 0; j < data.length - 1 - i; j++) {
                    if (Date.parse(data[j][p]) > Date.parse(data[j+1][p])) {
                        var temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
            return data;
        },
        // 日期倒序
        ReverseRankingDate(data, p) {
            for (var i = 0; i < data.length - 1; i++) {
                for (var j = 0; j < data.length - 1 - i; j++) {
                    if (Date.parse(data[j][p]) < Date.parse(data[j+1][p])) {
                        var temp = data[j];
                        data[j] = data[j + 1];
                        data[j + 1] = temp;
                    }
                }
            }
            return data;
        },
        compareDate(list,keyName){
            // 排序方法
            for (let i=0;i<list.length;i++){
                for (let j=i;j<list.length;j++){
                    if(new Date(list[i][keyName].replace(/-/,'/')) - new Date(list[j][keyName].replace(/-/,'/'))>0){
                        let listObj=list[i];
                        list[i]=list[j];
                        list[j]=listObj;
                    }
                }
            }
            return list;
        },
    } 