// 验证手机号码
export function checkPhoneNumber(rule, value, callback) {
    const reg = /^(0|86|17951)?(13[0-9]|15[012356789]|17[678]|18[0-9]|14[57])[0-9]{8}$/;
    if (!value) {
      return callback(new Error('请填写手机号码！'))
    } else if (!reg.test(value)) {
      return callback(new Error('请填写正确的手机号码！'))
    }else{
      callback()
    }
  }
  
  export function checkIdNum(rule, value, callback) {
    const reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/
    if (!value) {
      return callback(new Error('证件号码不能为空'))
    } else if (!reg.test(value)) {
      return callback(new Error('证件号码不正确'))
    } else {
      callback()
    }
  }
  export function checkEmail(rule, value, callback) {
    const reg  = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/
    if (!value) {
      return callback(new Error('邮箱不能为空'))
    } else if (!reg.test(value)) {
      return callback(new Error('邮箱地址不正确'))
    } else {
      callback()
    }
  }
  export default {
    checkPhone: [{ required: true, validator: checkPhoneNumber, trigger: 'blur' }],
    checkId: [{ required: true, validator: checkIdNum, trigger: 'blur' }],
  }
  