export default
  {
    dateFormat(time) {
      const dt = new Date(time)
      const y = dt.getFullYear()
      const m = (dt.getMonth() + 1 + '').padStart(2, '0')
      const d = (dt.getDate() + '').padStart(2, '0')

      const hh = (dt.getHours() + '').padStart(2, '0')
      const mm = (dt.getMinutes() + '').padStart(2, '0')
      const ss = (dt.getSeconds() + '').padStart(2, '0')

      const xingqi = (dt.getDay() + '')
      const weekday = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
      return `${y}/${m}/${d} ${hh}:${mm}:${ss}`
    }
  }