// 过滤器
const filter = {
  // 状态过滤器， 将数字0,1,2转化为对应文字状态
  // 状态(0：小甲鱼， 1： 带鱼， 2：美人鱼)
  deliverStatusFilter: function (val) {
    if (val == 0) {
      return '小甲鱼'
    } else if (val == 1) {
      return '带鱼'
    } else if (val == 2) {
      return '美人鱼'
    } else {
      console.log(val);
    }
  },

  timeFilter: function (originVal) {
	  // 第二个过滤器
    let dt = new Date(originVal)
    let y = dt.getFullYear()
    let m = (dt.getMonth() + 1 + '').padStart(2, '0')
    let d = (dt.getDate() + '').padStart(2, '0')
  
    let hh = (dt.getHours() + '').padStart(2, '0')
    let mm = (dt.getMinutes() + '').padStart(2, '0')
    let ss = (dt.getSeconds() + '').padStart(2, '0')
  
    return `${y}/${m}`
  },

  // 第3个过滤器 时间
  datetimeFilter : function (originVal) {
    let dt = new Date(originVal)
    let y = dt.getFullYear()
    let m = (dt.getMonth() + 1 + '').padStart(2, '0')
    let d = (dt.getDate() + '').padStart(2, '0')
  
    let hh = (dt.getHours() + '').padStart(2, '0')
    let mm = (dt.getMinutes() + '').padStart(2, '0')
    let ss = (dt.getSeconds() + '').padStart(2, '0')
  
    return `${y}-${m}-${d} ${hh}:${mm}:${ss}`
  },
    // 第4个过滤器 时间
  dateFilter : function (originVal) {
    let dt = new Date(originVal)
    let y = dt.getFullYear()
    let m = (dt.getMonth() + 1 + '').padStart(2, '0')
    let d = (dt.getDate() + '').padStart(2, '0')
    return `${y}-${m}-${d}`
  // 多个过滤器直接依次写在这里即可  
 }
}


export default filter